Feature: Smoke
  As a user
  I want to find pictures on Google
  So that I can be sure that site works correctly

  Scenario Outline: Search pictures on Google
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks on pictures tab button
    Then User checks that search page contains pictures on result

    Examples:
      | homePage               | keyword         |
      | https://www.google.com | Bam Bam Bigelow |
      | https://www.google.com | Undertaker      |
      | https://www.google.com | Doink the Clown |
