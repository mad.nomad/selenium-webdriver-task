package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {

    @FindBy(xpath = "//div[@class='hdtb-mitem']/a[contains(@href,'tbm=isch')]")
    private WebElement menuSearchPicturesButton;

    @FindBy(xpath = "//div[contains(@class,'isv-r')]//img")
    private List<WebElement> resultSearchPicturesListOfImages;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getMenuSearchPicturesButton() {
        return menuSearchPicturesButton;
    }

    public List<WebElement> getResultSearchPicturesListOfImages() {
        return resultSearchPicturesListOfImages;
    }

}
